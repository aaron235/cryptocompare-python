import aiohttp
from time import time


class CryptoCompare:
	def __init__( self, exchange='CCCAGG' ):
		self.__api_url = 'https://min-api.cryptocompare.com/data/'

		self.default_exchange = exchange
		self.request_timeout = 3

	async def apiReq( self, command, args={ } ):
		baseUrl = self.__api_url
		baseUrl += command
		async with aiohttp.ClientSession() as session:
			async with session.get( baseUrl, params=args ) as res:
				return await res.json()

	async def coinlist( self ):
		baseUrl = 'https://cryptocompare.com/api/data/'
		async with aiohttp.ClientSession() as session:
			async with session.get( baseUrl ) as res:
				return await res.json()

	def price( self, fromSym, toSyms, exchange=None ):
		# if the exchange is none, set it to the configured default
		exchange = self.default_exchange if exchange is None else exchange
		# if toSyms is a list, turn it into a comma-delimited string
		toSyms = ','.join( toSyms ) if type( toSyms ) is list else toSyms

		return self.apiReq( 'price', { 'fsym': fromSym, 'tsyms': toSyms, 'e': exchange } )

	def priceMulti( self, fromSyms, toSyms, exchange=None ):
		# if the exchange is none, set it to the configured default
		exchange = self.default_exchange if exchange is None else exchange
		# if toSyms or fromSyms are lists, turn them into a comma-delimited string
		toSyms = ','.join( toSyms ) if type( toSyms ) is list else toSyms
		fromSyms = ','.join( fromSyms ) if type( fromSyms ) is list else fromSyms

		return self.apiReq( 'pricemulti', { 'fsyms': fromSyms, 'tsyms': toSyms, 'e': exchange } )

	def priceMultiFull( self, fromSyms, toSyms, exchange=None ):
		# if the exchange is none, set it to the configured default
		exchange = self.default_exchange if exchange is None else exchange
		# if toSyms or fromSyms are lists, turn them into a comma-delimited string
		toSyms = ','.join( toSyms ) if type( toSyms ) is list else toSyms
		fromSyms = ','.join( fromSyms ) if type( fromSyms ) is list else fromSyms

		return self.apiReq( 'pricemultifull', { 'fsyms': fromSyms, 'tsyms': toSyms, 'e': exchange } )

	def generateAvg( self, fromSym, toSym, exchanges=None ):
		exchanges = self.default_exchange if exchanges is None else exchanges
		# If exchanges is a list, turn it into a comma-delimited string
		exchanges = ','.join( exchanges ) if type( exchanges ) is list else exchanges

		return self.apiReq( 'generateAvg', { 'fsym': fromSym, 'tsym': toSym, 'markets': exchanges } )

	def dayAvg( self, fromSym, toSym, exchange=None ):
		exchange = self.default_exchange if exchange is None else exchange

		return self.apiReq( 'dayAvg', { 'fsym': fromSym, 'tsym': toSym, 'e': exchange } )

	def priceHistorical( self, fromSym, toSym, timestamp=None, exchanges=None ):
		# set the timestamp to now if it was left blank
		timestamp = time() if timestamp is None else timestamp

		exchanges = self.default_exchange if exchanges is None else exchanges
		# If exchanges is a list, turn it into a comma-delimited string
		exchanges = ','.join( exchanges ) if type( exchanges ) is list else exchanges

		return self.apiReq( 'pricehistorical', { 'fsym': fromSym, 'tsym': toSym, 'ts': time, 'markets': exchanges } )

	def coinSnapshot( self, fromSym, toSym ):
		return self.apiReq( 'coinsnapshot', { 'fsym': fromSym, 'tsym': toSym } )

	def coinSnapshotFullById( self, id ):
		return self.apiReq( 'coinsnapshotfullbyid', { 'id': id } )

	def socialStats( self, id ):
		return self.apiReq( 'socialstats', { 'id': id } )

	def histoMinute( self, fromSym, toSym, exchange=None, limit=60 ):
		exchange = self.default_exchange if exchange is None else exchange

		return self.apiReq( 'histominute', { 'fsym': fromSym, 'tsym': toSym, 'e': exchange, 'limit': limit } )

	def histoHour( self, fromSym, toSym, exchange=None, limit=168 ):
		exchange = self.default_exchange if exchange is None else exchange

		return self.apiReq( 'histohour', { 'fsym': fromSym, 'tsym': toSym, 'e': exchange, 'limit': limit } )

	def histoDay( self, fromSym, toSym, exchange=None, limit=30 ):
		exchange = self.default_exchange if exchange is None else exchange

		return self.apiReq( 'histoday', { 'fsym': fromSym, 'tsym': toSym, 'e': exchange, 'limit': limit } )
