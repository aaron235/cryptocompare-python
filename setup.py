from setuptools import setup

setup(name='cryptocompare',
      version='0.1.2',
      description='Async API arapper for cryptocompare.com',
      url='https://gitlab.com/aaron235/cryptocompare-python.git',
      author='Aaron Adler',
      author_email='qwertyman159@gmail.com',
      license='MIT',
      packages=['cryptocompare'],
      install_requires=[
          'aiohttp',
      ],
      zip_safe=False)